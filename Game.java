import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Scanner;

public class Game {

	public Game() {
		// TODO Auto-generated constructor stub
	}

	public static void test(MyBoard b) {

		Scanner reader = new Scanner(System.in); // Reading from System.in
		HumanPlayer red = new HumanPlayer(Stone.RED, reader);
		HumanPlayer yellow = new HumanPlayer(Stone.YELLOW, reader);
		System.out.println(b);// display the current board
		HumanPlayer players[] = { red, yellow };
		int arr[][][] = { { { 9, 9 }, { 5, 13 }, { 7, 11 }, { 6, 12 }, { 8, 10 } },
				{ { 8, 8 }, { 10, 10 }, { 11, 11 }, { 12, 12 }, { 13, 13 } } };

		for (int i = 0; i < arr[0].length + arr[1].length; ++i) {
			int j = i % 2;
			Coordinate c = null;
			boolean ok = false;
			for (int k = 0; !ok; ++k) {
				// c = players[j ].getMove(b);
				int x = arr[j][i / 2][0];
				int y = arr[j][i / 2][1];
				c = new MyCoordinate(x, y);
				ok = b.checkCoordinate(c);
			}

			Stone s = players[j].getStone();
			b.placeStone(s, c);
			System.out.println(b);// display the board after the current move
			System.out.println(b.getWinner());// display the board after the current move
			System.out.println("winner = " + b.getWinnerOnDiagnol(5, 9));// display the board after the current move

		}
	}

	public static void yellowCaptureGameOver(MyBoard b) {
		b.placeStone(Stone.RED, new MyCoordinate(9, 9));
		b.placeStone(Stone.YELLOW, new MyCoordinate(9, 8));
		for (int i = 0; i < 5; i++) {
			if (i == 4) {
				i = 5;
			}
			b.placeStone(Stone.RED, new MyCoordinate(1, i));
			b.placeStone(Stone.YELLOW, new MyCoordinate(0, i));
			b.placeStone(Stone.RED, new MyCoordinate(2, i));
			b.placeStone(Stone.YELLOW, new MyCoordinate(3, i));
		    System.out.print( b );
			int x = b.getYellowCaptures();
			System.out.print(  x   );
			assertEquals(Integer.min(i + 1, 5), x );
			assertEquals(0, b.getRedCaptures());
			assertEquals(Stone.YELLOW, b.pieceAt(new MyCoordinate(0, i)));
			assertEquals(Stone.YELLOW, b.pieceAt(new MyCoordinate(3, i)));
			assertEquals(Stone.EMPTY, b.pieceAt(new MyCoordinate(1, i)));
			assertEquals(Stone.EMPTY, b.pieceAt(new MyCoordinate(2, i)));
		}

		assertTrue(b.gameOver());
		assertEquals(Stone.YELLOW, b.getWinner());
	}

	public static void main(String[] args) {

		MyBoard b = new MyBoard();

		yellowCaptureGameOver(b);

	}
}
