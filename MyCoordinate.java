public class MyCoordinate implements Coordinate {
	int row, col;
	public MyCoordinate(int a, int b) {
		
		row = a;
		col = b;
	}

	public int getRow() {
		return row;
	};

	public int getColumn() {
		return col;
	};

	public String toString() {
		return "(" + row + ", " + col + ")";
	};
}
