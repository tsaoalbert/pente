import java.util.Arrays;

class MyBoard implements Board {
	int moves = 0;
	int RED = 0;
	int YELLOW = 1;
	int captures[] = { 0, 0 }; // RED, YELLOW
	int size = 19;
	Stone[][] board = null;

	public MyBoard() {
		board = new Stone[size][size];
		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < size; ++j) {
				board[i][j] = Stone.EMPTY;
			}
		}
	}

	@Override
	public String toString() {
		String s = "\n\n\n";
		s += String.format("%3d", 0) + ":   ";
		for (int j = 0; j < size; ++j) {
			s += String.format("%3d", j);
		}
		s += "\n";
		for (int i = 0; i < size; ++i) {
			s += String.format("%3d", i) + ":   ";
			for (int j = 0; j < size; ++j) {
				Stone r = board[i][j];
				switch (r) {
				case RED:
					s += String.format("%3s", "X");
					break;
				case YELLOW:
					s += String.format("%3s", "O");
					break;
				case EMPTY:
					s += String.format("%3d", j);
					// s +=String.format("%3s", " ");

					break;
				default:
				}

			}
			s += "\n";
		}
		return s;
	}

	public boolean checkCoordinate(Coordinate c) {
		boolean ok = !isOutOfBounds(c);
		ok &= pieceAt(c) == Stone.EMPTY;
		ok &= (moves != 0 || inRange(c, 9, 10));// check the RED's first move
		ok &= !(moves == 2 && inRange(c, 6, 13));// illegal Red's 2nd move
		if (!ok) {
			System.out.println("Step " + moves + ": illegal move: " + c);
		}
		return ok;
	}

	public Stone getStone(Coordinate c) {
		return board[c.getRow()][c.getColumn()];
	}

	public void setStone(Coordinate c, Stone s) {
		 board[c.getRow()][c.getColumn()] = s;
	}

	public void setStone(Coordinate c) {
		 setStone(c, Stone.EMPTY);
	}

	@Override
	public void placeStone(Stone s, Coordinate c) {
		int x = c.getColumn();
		int y = c.getRow();
		if (!checkCoordinate(c)) {
			throw new RuntimeException();
			//return ;
		}
        setStone ( c, s);

		int k = (s == Stone.YELLOW) ? YELLOW : RED;
		Stone t = (s == Stone.YELLOW) ? Stone.RED : Stone.YELLOW;
		int x1, x2, x3, y1, y2, y3;
		int dx[][] = { { 1, 2, 3 }, { -1, -2, -3 }, { 0, 0, 0 }, { 0, 0, 0 } };
		int dy[][] = { { 0, 0, 0 }, { 0, 0, 0 }, { 1, 2, 3 }, { -1, -2, -3 }, };
		for (int i = 0; i < 4; ++i) {
			x1 = x + dx[i][0];
			x2 = x + dx[i][1];
			x3 = x + dx[i][2];
			y1 = y + dy[i][0];
			y2 = y + dy[i][1];
			y3 = y + dy[i][2];

			MyCoordinate c1 = new MyCoordinate(y1, x1);
			MyCoordinate c2 = new MyCoordinate(y2, x2);
			MyCoordinate c3 = new MyCoordinate(y3, x3);


			if (inRange(c3) && s == getStone(c3)) { // pt3 same color and in range
				System.out.println(Arrays.toString(captures));

				if (getStone(c1) == t && getStone(c2) == t) {
					captures[k] += 1; // xxx capture 1 pair
					System.out.println(Arrays.toString(captures));
					setStone(c1);
					setStone(c2);
				}
			}
		}

		// return if illegal
		// check if any captures
		// update the board
		moves++;
	}

	@Override
	public Stone pieceAt(Coordinate c) {
		// TODO Auto-generated method stub
		return board[c.getRow()][c.getColumn()];
	}

	boolean inRange(int key, int a, int n) {
		return key >= a && key < n;
	}

	public boolean inRange(Coordinate c, int a, int n) {
		// TODO Auto-generated method stub
		int y = c.getRow();
		int x = c.getColumn();
		return inRange(y, a, n) && inRange(x, a, n);
	}

	public boolean inRange(Coordinate c) {
		return inRange(c, 0, size);
	}

	@Override
	public boolean isOutOfBounds(Coordinate c) {
		return !inRange(c);
	}

	@Override
	public boolean isEmpty(Coordinate c) {
		// TODO Auto-generated method stub
		return pieceAt(c) == Stone.EMPTY;
	}

	@Override
	public int getMoveNumber() {
		// TODO Auto-generated method stub
		return moves;
	}

	@Override
	public int getRedCaptures() {
		// TODO Auto-generated method stub
		return captures[RED];
	}

	@Override
	public int getYellowCaptures() {
		// TODO Auto-generated method stub
		return captures[YELLOW];
	}

	public boolean boardFull() {
		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < size; ++j) {
				if (board[i][j] == Stone.EMPTY)
					return false;
			}
		}
		return true;
	}

	@Override
	public boolean gameOver() {
		// TODO Auto-generated method stub
		return getWinner() != Stone.EMPTY || boardFull();
	}

	public Stone getWinnerOnDiagnol(int i, int j) {
		if (i < size - 4) {
			boolean ok = true;
			boolean ok2 = true;
			Stone s = board[i][j];
			Stone s2 = board[i][j + 4];
			for (int k = 1; k < 5; ++k) {
				ok &= (s == board[i + k][j + k]);
				ok2 &= (s2 == board[i + k][j + 4 - k]);
			}
			if (ok)
				return s;
			if (ok2)
				return s2;
		}
		return Stone.EMPTY;
	}

	public Stone getWinner(int i, int j) {
		Stone sh = board[i][j];
		Stone sv = board[j][i];

		boolean okh = sh != Stone.EMPTY;
		boolean okv = sv != Stone.EMPTY;

		for (int k = 1; k < 5; ++k) {
			okh &= sh == board[i][j + k];
			okv &= sv == board[j + k][i];
		}
		if (okh)
			return sh;
		if (okv)
			return sv;

		return getWinnerOnDiagnol(i, j);
	}

	@Override
	public Stone getWinner() {

		if (captures[RED] >= 5)
			return Stone.RED;
		if (captures[YELLOW] >= 5)
			return Stone.YELLOW;

		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < size - 4; ++j) {
				Stone s = getWinner(i, j);
				if (s != Stone.EMPTY)
					return s;
			}
		}
		return Stone.EMPTY;
	}

};
