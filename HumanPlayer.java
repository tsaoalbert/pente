import java.util.Scanner;

public class HumanPlayer implements Player {

	Stone m_stone ;
	Scanner m_reader;
	public HumanPlayer(  Stone s, Scanner r) {
		m_stone = s;
		m_reader = r;
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public Coordinate getMove(Board b) {
		String s = m_stone==Stone.RED ? "RED": "YELLOW";
		System.out.print("Player " + s + ", please input row and column number: ");
		
		int row = m_reader.nextInt(); // Scans the next token of the input as an int.
		int col = m_reader.nextInt(); // Scans the next token of the input as an int.
		// reader.close();
		
		return new MyCoordinate (row,col);
	}

	@Override
	public Stone getStone() {
		// TODO Auto-generated method stub
		return m_stone;
	}

}
