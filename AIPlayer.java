import java.util.Scanner;

public class AIPlayer implements Player {

	Stone m_player ;
	public AIPlayer(  Stone s ) {
		m_player = s;
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public Coordinate getMove(Board b) {
		String s = b.getMoveNumber()%2==0 ? "RED": "YELLOW";
		System.out.println("Player " + s + ", please input row and column number");
		
		Scanner reader = new Scanner(System.in);  // Reading from System.in
		System.out.println("Enter a number: ");
		int row = reader.nextInt(); // Scans the next token of the input as an int.
		int col = reader.nextInt(); // Scans the next token of the input as an int.
		reader.close();
		
		return new MyCoordinate (row,col);
	}

	@Override
	public Stone getStone() {
		// TODO Auto-generated method stub
		return m_player;
	}

}
