
// package reference;
import static org.junit.Assert.*;

import org.junit.*;


public class MyBoardTest {
	Board b;
	Object o;

	@Before
	public void setup() {
		b = new MyBoard();
		o = new Object();
	}

	@Test
	public void testInit() {

		b.placeStone(Stone.RED, new MyCoordinate(9, 9));
		b.placeStone(Stone.YELLOW, new MyCoordinate(10, 9));
		b.placeStone(Stone.RED, new MyCoordinate(9, 12));
		assertFalse(b.gameOver());
		synchronized (o) {
			System.out.println("Init test--");
			System.out.println(b);
		}
		assertEquals(Stone.RED, b.pieceAt(new MyCoordinate(9, 9)));
		assertEquals(Stone.EMPTY, b.pieceAt(new MyCoordinate(10, 10)));
	}

	@Test
	public void testFiveInARow() {
		b.placeStone(Stone.RED, new MyCoordinate(9, 9));
		b.placeStone(Stone.YELLOW, new MyCoordinate(10, 9));
		b.placeStone(Stone.RED, new MyCoordinate(9, 13));
		b.placeStone(Stone.YELLOW, new MyCoordinate(10, 10));
		b.placeStone(Stone.RED, new MyCoordinate(9, 11));
		b.placeStone(Stone.YELLOW, new MyCoordinate(10, 11));
		b.placeStone(Stone.RED, new MyCoordinate(9, 12));
		b.placeStone(Stone.YELLOW, new MyCoordinate(10, 12));
		b.placeStone(Stone.RED, new MyCoordinate(9, 10));
		synchronized (o) {
			System.out.println("Five in a row center test, red wins");
			System.out.println(b);
		}
		assertTrue(b.gameOver());
		assertEquals(Stone.RED, b.getWinner());
	}

	@Test
	public void testDiagonalFiveInARow0() {
		b.placeStone(Stone.RED, new MyCoordinate(9, 9));
		b.placeStone(Stone.YELLOW, new MyCoordinate(10, 9));
		b.placeStone(Stone.RED, new MyCoordinate(13, 13));
		b.placeStone(Stone.YELLOW, new MyCoordinate(9, 10));
		b.placeStone(Stone.RED, new MyCoordinate(11, 11));
		b.placeStone(Stone.YELLOW, new MyCoordinate(10, 11));
		b.placeStone(Stone.RED, new MyCoordinate(12, 12));
		b.placeStone(Stone.YELLOW, new MyCoordinate(10, 12));
		b.placeStone(Stone.RED, new MyCoordinate(10, 10));
		synchronized (o) {
			System.out.println("Five in a row diagonal test, red wins");
			System.out.println(b);
		}
		assertTrue(b.gameOver());
		assertEquals(Stone.RED, b.getWinner());
	}

	@Test
	public void testDiagonalFiveInARow1() {
		b.placeStone(Stone.RED, new MyCoordinate(9, 9));
		b.placeStone(Stone.YELLOW, new MyCoordinate(10, 9));
		b.placeStone(Stone.RED, new MyCoordinate(5, 13));
		b.placeStone(Stone.YELLOW, new MyCoordinate(9, 10));
		b.placeStone(Stone.RED, new MyCoordinate(7, 11));
		b.placeStone(Stone.YELLOW, new MyCoordinate(10, 11));
		b.placeStone(Stone.RED, new MyCoordinate(6, 12));
		b.placeStone(Stone.YELLOW, new MyCoordinate(10, 12));
		b.placeStone(Stone.RED, new MyCoordinate(8, 10));
		synchronized (o) {
			System.out.println("Five in a row diagonal test, red wins");
			System.out.println(b);
		}
		assertTrue(b.gameOver());
		assertEquals(Stone.RED, b.getWinner());
	}

	@Test
	public void testRedCapture() {

		b.placeStone(Stone.RED, new MyCoordinate(9, 9));
		b.placeStone(Stone.YELLOW, new MyCoordinate(9, 8));
		b.placeStone(Stone.RED, new MyCoordinate(14, 14)); // necessary so as not to violate tournament rules
		b.placeStone(Stone.YELLOW, new MyCoordinate(9, 7));
		b.placeStone(Stone.RED, new MyCoordinate(9, 6));
		synchronized (o) {
			System.out.println("Testing a single red capture. Should see three red pieces and no yellow");
			System.out.println(b);
		}
		assertEquals(Stone.RED, b.pieceAt(new MyCoordinate(9, 9)));
		assertEquals(Stone.RED, b.pieceAt(new MyCoordinate(9, 6)));
		assertEquals(Stone.EMPTY, b.pieceAt(new MyCoordinate(9, 8)));
		assertEquals(Stone.EMPTY, b.pieceAt(new MyCoordinate(9, 7)));
		assertEquals(1, b.getRedCaptures());
		assertEquals(0, b.getYellowCaptures());
		assertFalse(b.gameOver());
	}

	@Test
	public void testYellowCapture() {
		b.placeStone(Stone.RED, new MyCoordinate(9, 9));
		b.placeStone(Stone.YELLOW, new MyCoordinate(9, 8));
		b.placeStone(Stone.RED, new MyCoordinate(14, 14)); // necessary so as not to violate tournament rules
		b.placeStone(Stone.YELLOW, new MyCoordinate(14, 15));
		b.placeStone(Stone.RED, new MyCoordinate(9, 10));
		b.placeStone(Stone.YELLOW, new MyCoordinate(9, 11));
		synchronized (o) {
			System.out.println(
					"Testing a single yellow capture. Should only see 1 red piece, after yellow executed a capture");
			System.out.println(b);
		}
		assertEquals(Stone.YELLOW, b.pieceAt(new MyCoordinate(9, 11)));
		assertEquals(Stone.YELLOW, b.pieceAt(new MyCoordinate(9, 8)));
		assertEquals(Stone.EMPTY, b.pieceAt(new MyCoordinate(9, 10)));
		assertEquals(Stone.EMPTY, b.pieceAt(new MyCoordinate(9, 9)));
		assertEquals(0, b.getRedCaptures());
		assertEquals(1, b.getYellowCaptures());
		assertFalse(b.gameOver());

	}

	@Test
	public void redCaptureGameOver() {
		b.placeStone(Stone.RED, new MyCoordinate(9, 9));
		b.placeStone(Stone.YELLOW, new MyCoordinate(9, 8));
		b.placeStone(Stone.RED, new MyCoordinate(14, 14));
		for (int i = 0; i < 5; i++) {
			if (i == 4) {
				i = 5;
			}
			b.placeStone(Stone.YELLOW, new MyCoordinate(i, 1));
			b.placeStone(Stone.RED, new MyCoordinate(i, 0));
			b.placeStone(Stone.YELLOW, new MyCoordinate(i, 2));
			b.placeStone(Stone.RED, new MyCoordinate(i, 3));
			assertEquals(Integer.min(i + 1, 5), b.getRedCaptures());
			assertEquals(0, b.getYellowCaptures());
			assertEquals(Stone.RED, b.pieceAt(new MyCoordinate(i, 0)));
			assertEquals(Stone.RED, b.pieceAt(new MyCoordinate(i, 3)));
			assertEquals(Stone.EMPTY, b.pieceAt(new MyCoordinate(i, 1)));
			assertEquals(Stone.EMPTY, b.pieceAt(new MyCoordinate(i, 2)));
		}
		synchronized (o) {
			System.out.println("Testing red 5 captures");
			System.out.println(b);
		}
		assertTrue(b.gameOver());
		assertEquals(Stone.RED, b.getWinner());
	}

	@Test
	public void yellowCaptureGameOver() {
		b.placeStone(Stone.RED, new MyCoordinate(9, 9));
		b.placeStone(Stone.YELLOW, new MyCoordinate(9, 8));
		for (int i = 0; i < 5; i++) {
			if (i == 4) {
				i = 5;
			}
			b.placeStone(Stone.RED, new MyCoordinate(1, i));
			b.placeStone(Stone.YELLOW, new MyCoordinate(0, i));
			b.placeStone(Stone.RED, new MyCoordinate(2, i));
			b.placeStone(Stone.YELLOW, new MyCoordinate(3, i));
			assertEquals(Integer.min(i + 1, 5), b.getYellowCaptures());
			assertEquals(0, b.getRedCaptures());
			assertEquals(Stone.YELLOW, b.pieceAt(new MyCoordinate(0, i)));
			assertEquals(Stone.YELLOW, b.pieceAt(new MyCoordinate(3, i)));
			assertEquals(Stone.EMPTY, b.pieceAt(new MyCoordinate(1, i)));
			assertEquals(Stone.EMPTY, b.pieceAt(new MyCoordinate(2, i)));
		}
		synchronized (o) {
			System.out.println("Testing yellow 5 captures");
			System.out.println(b);
		}
		assertTrue(b.gameOver());
		assertEquals(Stone.YELLOW, b.getWinner());
	}

	@Test
	public void testSideFive() {
		b.placeStone(Stone.RED, new MyCoordinate(9, 9));
		b.placeStone(Stone.YELLOW, new MyCoordinate(0, 0));
		b.placeStone(Stone.RED, new MyCoordinate(10, 12));
		b.placeStone(Stone.YELLOW, new MyCoordinate(0, 1));
		b.placeStone(Stone.RED, new MyCoordinate(11, 13));
		b.placeStone(Stone.YELLOW, new MyCoordinate(0, 3));
		b.placeStone(Stone.RED, new MyCoordinate(12, 15));
		b.placeStone(Stone.YELLOW, new MyCoordinate(0, 4));
		b.placeStone(Stone.RED, new MyCoordinate(13, 17));
		b.placeStone(Stone.YELLOW, new MyCoordinate(0, 2));
		synchronized (o) {
			System.out.println("Testing 5 in a row on the side");
			System.out.println(b);
		}
		assertTrue(b.gameOver());
		assertEquals(Stone.YELLOW, b.getWinner());
	}

	@Test(expected = RuntimeException.class)
	public void testIllegalFirstMove0() {
		b.placeStone(Stone.RED, new MyCoordinate(9, 8));
	}

	@Test(expected = RuntimeException.class)
	public void testIllegalFirstMove1() {
		b.placeStone(Stone.RED, new MyCoordinate(8, 9));
	}

	@Test(expected = RuntimeException.class)
	public void testIllegalSecondMove0() {
		b.placeStone(Stone.RED, new MyCoordinate(9, 9));
		b.placeStone(Stone.YELLOW, new MyCoordinate(9, 8));
		b.placeStone(Stone.RED, new MyCoordinate(9, 11));
	}

	@Test(expected = RuntimeException.class)
	public void testIllegalSecondMove1() {
		b.placeStone(Stone.RED, new MyCoordinate(9, 9));
		b.placeStone(Stone.YELLOW, new MyCoordinate(9, 8));
		b.placeStone(Stone.RED, new MyCoordinate(11, 9));

	}
	
	public void main (String args[]) {
		MyBoard b = new MyBoard ();
		System.out.println( "---" + b); 	
	}
}
